package com.sharon.model;

import java.util.Date;

/**
 * Created by orensharon on 3/10/17.
 */
public class SimplePerson extends AbstractPerson {

    public SimplePerson(int id, String firstName, String lastName, int height, Date dateOfBirth) {
        super(id, firstName, lastName, height, dateOfBirth);
    }
}
