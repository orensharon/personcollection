package com.sharon.model;

import java.util.Date;

/**
 * Created by orensharon on 3/10/17.
 */
public class AbstractPerson implements Person {

    private int id, height;
    private String firstName, lastName;
    private Date dateOfBirth;

    public AbstractPerson(int id, String firstName, String lastName, int height, Date dateOfBirth) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.height = height;
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "AbstractPerson{" +
                "id=" + id +
                ", height=" + height +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                "}\n";
    }
}
