package com.sharon.model;

import java.util.Date;

/**
 * Created by orensharon on 3/10/17.
 */
public class CrazyPerson extends AbstractPerson {

    private int craziness;

    public CrazyPerson(int id, String firstName, String lastName, int height, Date dateOfBirth, int craziness) {
        super(id, firstName, lastName, height, dateOfBirth);
        craziness = craziness;
    }

    public int getCraziness() {
        return craziness;
    }
}
