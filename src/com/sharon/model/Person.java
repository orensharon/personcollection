package com.sharon.model;

import java.util.Date;

/**
 * Created by orensharon on 3/10/17.
 */
public interface Person {
    int getId();
    String getFirstName();
    String getLastName();
    Date getDateOfBirth();
    int getHeight();
}
