package com.sharon;

import com.sharon.model.Person;

import java.util.Observer;

/**
 * Created by orensharon on 3/10/17.
 */
public interface PersonCollection {
    void add(Person newPerson);
    Person remove();
    int compare(Person p1, Person p2);

    void subscribe(Observer observer);
    void unSubscribe(Observer observer);

}
