package com.sharon;

import com.sharon.model.CrazyPerson;
import com.sharon.model.Person;
import com.sharon.model.SimplePerson;

import java.util.Date;
import java.util.Observable;
import java.util.Observer;

public class Main {

    public static void main(String[] args) {


        // Create person collection with implementation of getValue
        PersonCollection personCollection = new PersonCollectionImpl();

        // Add a subscriber
        Observer observer = new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                if (arg != null) {
                    Person person = (Person) arg;
                    System.out.println("UPDATE:" +  person.toString());
                }
            }
        };
        personCollection.subscribe(observer);

        // Create persons
        Person person1 = new SimplePerson(4, "Lara", "Croft", 180, new Date());
        Person person2 = new SimplePerson(2, "Bugs", "Bunny", 160, new Date());
        Person person3 = new CrazyPerson(1, "Michael Jackson", "Aspir", 112, new Date(), 10);
        Person person4 = new CrazyPerson(3, "DJ", "Tiesto", 128, new Date(), 2);
        personCollection.add(person1);
        personCollection.add(person2);
        personCollection.add(person3);
        personCollection.add(person4);

        System.out.println(personCollection);

        personCollection.remove();
        personCollection.remove();
        personCollection.remove();
        personCollection.remove();

        System.out.println(personCollection);
        personCollection.unSubscribe(observer);


    }
}
