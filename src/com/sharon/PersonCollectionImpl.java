package com.sharon;

import com.sharon.model.Person;

import java.util.*;

/**
 * Created by orensharon on 3/10/17.
 */
public class PersonCollectionImpl extends Observable implements PersonCollection {

    private List<Person> persons;

    public PersonCollectionImpl() {
        // Initiate thread-safe array list
        persons = Collections.synchronizedList(new LinkedList<Person>());
    }

    @Override
    public synchronized void add(Person newPerson) {

        if (newPerson == null) return;
        if (persons.size() == 0) {
            // Insert first person - no manipulation needed
            persons.add(newPerson);
            publish(newPerson);
        } else {
            // Add person to list.
            // The greater person's value - greater in list
            int index = 0;
            for (Person person : persons) {
                int comparator = compare(person, newPerson);
                if (comparator <= 0) {
                    persons.add(index, newPerson);
                    publish(newPerson);
                    return;
                }
                index ++;
            }
            // Current new person has maximum value
            persons.add(newPerson);
            publish(newPerson);
        }
    }

    @Override
    public synchronized Person remove() {
        int maximumIndex = persons.size() -1;
        if (maximumIndex == -1) return null;
        // Removing last node in linked list is in o(1)
        Person result = persons.remove(maximumIndex);
        publish(result);
        return result;
    }

    @Override
    public void subscribe(Observer observer) {
        if (observer == null) return;
        addObserver(observer);
    }

    @Override
    public void unSubscribe(Observer observer) {
        if (observer == null) return;
        deleteObserver(observer);
    }


    private void publish(Person person) {
        setChanged();
        notifyObservers(person);
    }


    /**
     * Comparing between two given persons
     * @param p1 first person
     * @param p2 second person
     * @return -1 if p1 has greater value then p2, 1 otherwise, 0 if equals
     */
    @Override
    public int compare(Person p1, Person p2) {
        // TODO: Should be implemented
        return 0;
    }


    @Override
    public String toString() {
        return "PersonCollectionImpl{" +
                "persons=" + persons +
                '}';
    }
}
